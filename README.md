# Container Image CI Templates

Templates for building container images using GitLabs CI/CD pipelines for CMS.

## Kaniko image builder

See [`kaniko-image.gitlab-ci.yml`](./kaniko-image.gitlab-ci.yml).
This is taken from https://gitlab.cern.ch/ci-tools/container-image-ci-templates/-/blob/master/kaniko-image.gitlab-ci.yml.

## Skopeo image tagger

See [``skopeo.gitlab-ci.yml``](./skopeo.gitlab-ci.yml).
This uses Skopeo to tag a container image without rebuilding.
